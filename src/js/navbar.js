const hamburgerNode = document.querySelector('.navbar__hamburger');
const navbarNode = document.querySelector('.navbar');

const toggleNavbar = function (e) {
  hamburgerNode.classList.toggle('navbar__hamburger--active');
  navbarNode.classList.toggle('navbar--opened');

  e.preventDefault();
};

hamburgerNode.addEventListener('click', toggleNavbar);
