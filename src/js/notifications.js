const DEFAULT_NOTIFY_TIMEOUT = 4000;

export default class Notifications {
  constructor() {
    this.rootNode = document.querySelector('.notifications');

    if (!this.rootNode) throw new Error('No notifications root element');

    this.template = (text) => {
      return `
      <div class="notifications__item">
        <div class="notifications__item-title">
          ${text}
        </div>

        <button class="notifications__item-close">
          <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
            <rect width="2" height="12" rx="1"
              transform="matrix(-0.707107 -0.707107 -0.707107 0.707107 14.9498 6.46447)"
              fill="white"
            />
            <rect width="2" height="12" rx="1"
              transform="matrix(-0.707107 0.707107 0.707107 0.707107 6.46446 5.05025)"
              fill="white"
            />
          </svg>
        </button>
      </div>
    `;
    };
  }

  createNotificationElement(text) {
    const html = this.template(text).trim();
    const template = document.createElement('template');
    template.innerHTML = html;
    return template.content.firstChild;
  }

  notify(text, timeout = DEFAULT_NOTIFY_TIMEOUT) {
    if (!text) throw new Error('Unable to create a notification without text');

    const notification = this.createNotificationElement(text);
    this.rootNode.append(notification);

    setTimeout(function destroyNotification() {
      notification.remove();
    }, timeout);
  }
}
