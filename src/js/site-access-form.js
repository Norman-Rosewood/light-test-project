import Notifications from './notifications';

const notifications = new Notifications();

const siteAccessForm = document.querySelector('form');
const passwordInput = document.querySelector('#site-access-password');

const SUCCESS_CHANGES_NOTIFY = 'Настройки доступа к сайту успешно изменены';
const NOTIFY_TIMEOUT = 4000;

siteAccessForm.addEventListener(
  'change',
  function togglePasswordInputVisibility(e) {
    if (e.target.type !== 'radio') return;
    const radio = e.target;

    if (radio.getAttribute('value') === 'password') {
      passwordInput.classList.remove('hiden');
    } else {
      passwordInput.classList.add('hiden');
    }
  }
);

siteAccessForm.addEventListener('submit', function submitSiteAccessForm(e) {
  notifications.notify(SUCCESS_CHANGES_NOTIFY, NOTIFY_TIMEOUT);

  e.preventDefault();
});
